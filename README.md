# Terraform 101

This repository is used to demonstrate Terraform basics.

Read about it [over here at the OpsFactory blog](https://opsfactory.com.au/terraform/).