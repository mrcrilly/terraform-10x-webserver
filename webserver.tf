# OpsFactory Pty Ltd
# Author: Michael Crilly <mike@opsfactory.com.au>
# Terraform: 0.12
#
# Designed to be used with the OpsFactory terraform 10X
# Brown Bag training session.

# Variables allow us to define facts we can repeat throughout
# our code without having to hard code them. They can be changed
# in a single location and the change will be propagated...
variable "tag_name" {
  default = "terraform10x-change"
}

# The Provider{} block configures Terraform with regards to
# the service provider we're going to use. We're using
# AWS in this example.
provider "aws" {
  region = "ap-southeast-2"
}

variable "aws_region" {
  default = "ap-southeast-2"
}

# Here we're configuring the Terraform backend, which
# stores our infrastructure's state in S3. It also configures
# a DynamoDB Table to be used for locking state...
terraform {
  backend "s3" {
    bucket = "terraform-10x-brownbag-state"
    key    = "webserver/terraform.tfstate"
    region = "ap-southeast-2"
    dynamodb_table = "terraform-10x-brownbag-lock"
  }
}

# VPC
resource "aws_vpc" "test" {
  cidr_block = "10.1.0.0/16"
  tags = {
    Name = var.tag_name
  }
}

# Subnet
resource "aws_subnet" "webserver" {
  vpc_id     = aws_vpc.test.id
  cidr_block = "10.1.1.0/24"
  availability_zone = "ap-southeast-2a"

  tags = {
    Name = "${var.tag_name}-webserver"
  }
}

# Route Table
resource "aws_route_table" "default" {
  vpc_id = aws_vpc.test.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = var.tag_name
  }
}

# This associates our route table to our subnet, making
# the subnet use the route table for its routing rules.
resource "aws_route_table_association" "default" {
  subnet_id      = aws_subnet.webserver.id
  route_table_id = aws_route_table.default.id
}

# Network ACL: we just allow everything
resource "aws_network_acl" "webserver" {
  vpc_id = aws_vpc.test.id

  egress {
    protocol   = "-1"
    rule_no    = 200
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name = var.tag_name
  }
}

# Internet Gateway so that we can talk over the public
# Internet.
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.test.id

  tags = {
    Name = var.tag_name
  }
}

# Security Group: 80 and 22 only, plus all egress.
resource "aws_security_group" "protected" {
  name        = "protected"
  description = "Only allows HTTP and SSH"
  vpc_id      = aws_vpc.test.id

  ingress {
    # TLS (change to whatever ports you need)
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    # TLS (change to whatever ports you need)
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

# An EIP isn't strictly required, but I like
# ensure the IP is static and in my control as
# and when I can.
resource "aws_eip" "webserver" {
  instance = aws_instance.webserver.id
  vpc      = true
  depends_on = ["aws_internet_gateway.igw"]
}

# Keypair: replace with your own PUBLIC key.
resource "aws_key_pair" "webserver" {
  key_name   = "webserver-keypair"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCeM1DauKqn+PeQ6uBlPYg41pIwyI4AjxR1Nfbk/n7R83n8Qywc40sPZjAcow/UG9rlhPerd0/MKL8eFRbCUtfjkMucPgPpmbbP2MVM29WBbHi0NLjamS4PwNjP9uMdBUzxlt1TZCPfMtqQF6gx899aCPhF8GbQb82wP2MV/tMDThT2cffwI8ElcizNZ5gz7psqzXYm6+DhrEhu2ii70mXhqmE0bx1ATLBxShtHXtuj4vSdkf567lcj4kkwT7DD5o1V7LQbSCe1JDqdqsNQsTVYuUW7wbsxNhIL5jlVbpVdCwK7OGB3cd2JUXf7wrpJdigyfzu5nUOb8f/dz4hEYK0/ michaelc@michaels-mbp"
}

# Using a custom AMI, build with Packer, is a better option than this...
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

# A simple EC2 Instance. Note how we're using all the resources
# we defined and created above. Very little is hard coded and
# what is hard coded could easily be abstracted into variables
resource "aws_instance" "webserver" {
  ami                     = data.aws_ami.ubuntu.id
  availability_zone       = var.aws_region
  instance_type           = "t2.large"
  key_name                = aws_key_pair.webserver.key_name
  vpc_security_group_ids  = [aws_security_group.protected.id]
  subnet_id               = aws_subnet.webserver.id
  
  root_block_device {
    volume_size = 50
  }

  tags = {
    Name = "${var.tag_name}-webserver-01"
  }
}

# Outputs allow us to expose internal Terraform state,
# such as public (or private) IP addresses and other
# key information. This simple output gives us the
# EC2 Instance's public IP so that we don't have to
# hunt for it in the AWS Console.
output "instance_ip_addr" {
  value = aws_eip.webserver.public_ip
}
